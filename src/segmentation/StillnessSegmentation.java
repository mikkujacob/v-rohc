/**
 * 
 */
package segmentation;

import java.util.ArrayList;

import shared.Body;
import shared.Clock;

/**
 * @author mikhailjacob
 *
 */
public class StillnessSegmentation
{
	private static final double ALPHA = 0.25;
	private static final double STILLNESS_VELOCITY_THRESHOLD = 40.0;
	private static final double SEGMENT_TIME_THRESHOLD = 0.75;

	private Clock clock;
	private boolean hasStarted;
	private boolean isSegmentDetected;
	private ArrayList<Body> currentBodyStream;
	private ArrayList<Double> currentTimestampStream;
	private ArrayList<Double> currentVelocityStream;
	private ArrayList<Double> currentVelocityStreamSmoothed;
	private ArrayList<Body> segment;
	private int startIndex;
	private int endIndex;
	private float segmentTime;

	public StillnessSegmentation()
	{
		clock = new Clock();
		hasStarted = false;
		isSegmentDetected = false;
		currentBodyStream = new ArrayList<Body>();
		currentTimestampStream = new ArrayList<Double>();
		currentVelocityStream = new ArrayList<Double>();
		currentVelocityStreamSmoothed = new ArrayList<Double>();
		startIndex = -1;
		endIndex = -1;
		segmentTime = 0f;
	}
	
	public void reset()
	{
		clock.start();
		hasStarted = false;
		isSegmentDetected = false;
		currentBodyStream.clear();
		currentTimestampStream.clear();
		currentVelocityStream.clear();
		currentVelocityStreamSmoothed.clear();
		startIndex = -1;
		endIndex = -1;
		segmentTime = 0f;
		segment = null;
	}

	public void update(Body pose)
	{
		clock.check();

		Body newPose = (Body) pose.clone();
		newPose.setTimestamp(clock.time());

		double velocity = calculateVelocity(newPose, currentBodyStream);
		double smoothedVelocity = smootheEMA(velocity,
				this.currentVelocityStreamSmoothed, ALPHA);

		currentBodyStream.add(newPose);
		currentTimestampStream.add((double) newPose.getTimestamp());
		currentVelocityStream.add(velocity);
		currentVelocityStreamSmoothed.add(smoothedVelocity);

		detectSegment(currentVelocityStreamSmoothed,
				STILLNESS_VELOCITY_THRESHOLD, SEGMENT_TIME_THRESHOLD);
	}

	public double calculateVelocity(Body pose, ArrayList<Body> poseHistory)
	{
		if (poseHistory.isEmpty())
		{
			return 0.0;
		}
		else
		{
			double deltaPose = (poseHistory.get(poseHistory.size() - 1).difference(
					pose) / 100.0);
			double deltaTime = Math.abs(poseHistory.get(poseHistory.size() - 1).getTimestamp()
					- pose.getTimestamp());
			return deltaPose / deltaTime;
		}
	}

	public static double smootheEMA(Double currentValue,
			ArrayList<Double> smoothedValueHistory, double alpha)
	{
		if (smoothedValueHistory.isEmpty())
		{
			// EMA0 = X0
			return currentValue;
		}
		else
		{
			// EMAt = ALPHA x Xt + (1 - ALPHA) x EMAt-1
			return alpha * currentValue + (1 - alpha)
					* smoothedValueHistory.get(smoothedValueHistory.size() - 1);
		}
	}

	public void detectSegment(ArrayList<Double> smoothedVelocityHistory,
			double stillnessVelocityThreshold, double segmentTimeThreshold)
	{
		if (hasStarted == false)
		{
			// Detect start
			if (smoothedVelocityHistory.get(smoothedVelocityHistory.size() - 1) > stillnessVelocityThreshold)
			{
				hasStarted = true;
				startIndex = smoothedVelocityHistory.size() - 1;
				segmentTime = 0f;
			}
		}
		if (hasStarted == true)
		{
			segmentTime = currentBodyStream.get(
					smoothedVelocityHistory.size() - 1).getTimestamp()
					- currentBodyStream.get(startIndex).getTimestamp();

			// Detect end
			if (smoothedVelocityHistory.get(smoothedVelocityHistory.size() - 1) < stillnessVelocityThreshold)
			{
				endIndex = smoothedVelocityHistory.size() - 1;

				// Long enough segment detected
				if (currentBodyStream.get(endIndex).getTimestamp()
						- currentBodyStream.get(startIndex).getTimestamp() > segmentTimeThreshold)
				{
					segment = new ArrayList<Body>();
					for (int index = startIndex; index < endIndex + 1; index++)
					{
						segment.add(currentBodyStream.get(index));
					}
					isSegmentDetected = true;
				}

				hasStarted = false;
				startIndex = -1;
				endIndex = -1;
			}
		}
	}

	public boolean isSegmentDetected()
	{
		return isSegmentDetected;
	}

	public ArrayList<Body> consumeSegment()
	{
		isSegmentDetected = false;
		return segment;
	}

	public double getFirstTimeStamp()
	{
		return currentBodyStream.get(0).getTimestamp();
	}

	public double getLastTimeStamp()
	{
		return currentBodyStream.get(currentBodyStream.size() - 1).getTimestamp();
	}

	public double getLastVelocity()
	{
		return currentVelocityStream.get(currentVelocityStream.size() - 1);
	}

	public double getLastVelocitySmoothed()
	{
		return currentVelocityStreamSmoothed.get(currentVelocityStreamSmoothed.size() - 1);
	}

	public float getSegmentTime()
	{
		return segmentTime;
	}

	public String getParameters()
	{
		String text = "";
		text += "\nAlpha: " + ALPHA;
		text += "\nStillness Velocity Threshold: "
				+ STILLNESS_VELOCITY_THRESHOLD;
		text += "\nSegment Time Threshold: " + SEGMENT_TIME_THRESHOLD;
		return text;
	}

	public ArrayList<Body> getCurrentBodyStream()
	{
		return currentBodyStream;
	}

	public ArrayList<Double> getCurrentVelocityStream()
	{
		return currentVelocityStream;
	}

	public ArrayList<Double> getCurrentVelocityStreamSmoothed()
	{
		return currentVelocityStreamSmoothed;
	}

	public ArrayList<Double> getCurrentTimestampStream()
	{
		return currentTimestampStream;
	}

	public boolean isEmpty()
	{
		return (currentBodyStream.isEmpty() || currentTimestampStream.isEmpty()
				|| currentVelocityStream.isEmpty() || currentVelocityStreamSmoothed.isEmpty());
	}
}
