package shared;

import processing.core.PVector;

public class Plane
{
	public Plane(PVector origin, PVector norm)
	{
		this.origin = origin;
		this.norm = norm;
	}

	public PVector origin;
	public PVector norm;
}
