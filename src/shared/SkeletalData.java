package shared;

import java.util.HashMap;

import processing.core.PVector;

public abstract class SkeletalData extends HashMap<JIDX, PVector>
{
	public static final long serialVersionUID = 4881124592186923286L;

	private float timestamp = 0.0f;

	public float getTimestamp()
	{
		return timestamp;
	}

	public void setTimestamp(float timestamp)
	{
		this.timestamp = timestamp;
	}

	@Override
	public PVector get(Object jidxObject)
	{
		JIDX jidx = (JIDX) jidxObject;
		if (JIDX.LEFT_COLLAR == jidx || JIDX.RIGHT_COLLAR == jidx)
		{
			return super.get(JIDX.NECK);
		}
		else if (JIDX.WAIST == jidx)
		{
			return super.get(JIDX.TORSO);
		}
		else if (JIDX.LEFT_ANKLE == jidx)
		{
			return super.get(JIDX.LEFT_FOOT);
		}
		else if (JIDX.RIGHT_ANKLE == jidx)
		{
			return super.get(JIDX.RIGHT_FOOT);
		}
		else
		{
			return super.get(jidx);
		}
	}
}
