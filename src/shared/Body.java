package shared;

import java.util.ArrayList;
import java.util.Map;
import java.io.Serializable;

import SimpleOpenNI.SimpleOpenNI;
import processing.core.*;

public class Body extends SkeletalData implements Serializable, Cloneable
{

	/**
	 * Serial Version UID - DO NOT CHANGE PLEASE!!!
	 */
	private static final long serialVersionUID = 2610605569934643630L;

	public void set(JIDX iJidx, PVector iPvector)
	{
		put(iJidx, iPvector);
	}

	public void initialize(SimpleOpenNI context, int userId, float timestamp)
	{
		setTimestamp(timestamp);

		PVector jointPos = new PVector();

		// note: jointPos.get() is copying each PVector as they are
		// call-by-reference

		// Get Skeleton Coordinates
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_HEAD,
				jointPos);
		set(JIDX.HEAD, jointPos.get());
		// PApplet.print(joints.get(JIDX.HEAD)+"\n");
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_NECK,
				jointPos);
		set(JIDX.NECK, jointPos.get());
		// context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_NECK,jointPos);
		// //Never used this before
		// set(JIDX.LEFT_COLLAR,jointPos.get());
		context.getJointPositionSkeleton(userId,
				SimpleOpenNI.SKEL_LEFT_SHOULDER, jointPos);
		set(JIDX.LEFT_SHOULDER, jointPos.get());
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_LEFT_ELBOW,
				jointPos);
		set(JIDX.LEFT_ELBOW, jointPos.get());
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_LEFT_HAND,
				jointPos);
		set(JIDX.LEFT_HAND, jointPos.get());
		context.getJointPositionSkeleton(userId,
				SimpleOpenNI.SKEL_LEFT_FINGERTIP, jointPos); // always (0.0,
																// 0.0, 0.0)
		set(JIDX.LEFT_FINGERTIP, jointPos.get());
		// context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_NECK,jointPos);
		// //Never used this before
		// set(JIDX.RIGHT_COLLAR,jointPos.get());
		context.getJointPositionSkeleton(userId,
				SimpleOpenNI.SKEL_RIGHT_SHOULDER, jointPos);
		set(JIDX.RIGHT_SHOULDER, jointPos.get());
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_RIGHT_ELBOW,
				jointPos);
		set(JIDX.RIGHT_ELBOW, jointPos.get());
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_RIGHT_HAND,
				jointPos);
		set(JIDX.RIGHT_HAND, jointPos.get());
		context.getJointPositionSkeleton(userId,
				SimpleOpenNI.SKEL_RIGHT_FINGERTIP, jointPos); // always (0.0,
																// 0.0, 0.0)
		set(JIDX.RIGHT_FINGERTIP, jointPos.get());
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_TORSO,
				jointPos);
		set(JIDX.TORSO, jointPos.get());
		// context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_TORSO,jointPos);
		// // always (0.0, 0.0, 0.0)
		// set(JIDX.WAIST,jointPos.get());
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_LEFT_HIP,
				jointPos);
		set(JIDX.LEFT_HIP, jointPos.get());
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_LEFT_KNEE,
				jointPos);
		set(JIDX.LEFT_KNEE, jointPos.get());
		// context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_FOOT,jointPos);
		// // always (0.0, 0.0, 0.0)
		// set(JIDX.LEFT_ANKLE,jointPos.get());
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_LEFT_FOOT,
				jointPos);
		set(JIDX.LEFT_FOOT, jointPos.get());
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_RIGHT_HIP,
				jointPos);
		set(JIDX.RIGHT_HIP, jointPos.get());
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_RIGHT_KNEE,
				jointPos);
		set(JIDX.RIGHT_KNEE, jointPos.get());
		// context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_FOOT,jointPos);
		// // always (0.0, 0.0, 0.0)
		// set(JIDX.RIGHT_ANKLE,jointPos.get());
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_RIGHT_FOOT,
				jointPos);
		set(JIDX.RIGHT_FOOT, jointPos.get());
	}

	public PVector[] localBasis()
	{
		PVector localFwddir = PVecUtilities.orthonormalization(orientation(),
				PVecUtilities.UPVEC);
		PVector localSidedir = PVecUtilities.UPVEC.cross(localFwddir);
		PVector[] localBasis =
		{ localSidedir, PVecUtilities.UPVEC, localFwddir };
		return localBasis;
	}

	public PVector center()
	{
		return get(JIDX.TORSO);
	}

	public Body transformed(PVector[] sourceBasis, PVector[] targetBasis,
			PVector targetCenter)
	{
		Body transformedPose = new Body();
		PVector center = center();
		for (JIDX jidx : JIDX.ALL_JIDX)
		{
			if (JIDX.TORSO == jidx)
			{
				transformedPose.set(JIDX.TORSO, targetCenter);
			}
			else
			{
				PVector jointPosition = get(jidx);
				PVector radiusVector = PVector.sub(jointPosition, center);
				float[] dissolution = PVecUtilities.dissolve(radiusVector,
						sourceBasis);
				PVector normalizedPosition = PVecUtilities.assemble(
						targetBasis, dissolution);
				transformedPose.set(jidx,
						PVector.add(targetCenter, normalizedPosition));
			}
		}
		transformedPose.setTimestamp(getTimestamp());
		return transformedPose;
	}

	public Body translated(PVector targetCenter)
	{
		Body translatedPose = new Body();
		PVector center = center();
		for (JIDX jidx : JIDX.ALL_JIDX)
		{
			if (JIDX.TORSO == jidx)
			{
				translatedPose.set(jidx, targetCenter);
			}
			else
			{
				PVector jointPosition = get(jidx);
				PVector radiusVector = PVector.sub(jointPosition, center);
				translatedPose.set(jidx,
						PVector.add(targetCenter, radiusVector));
			}
		}
		return translatedPose;
	}

	public PVector orientation()
	{
		return PVecUtilities.normTo3Pt(get(JIDX.TORSO),
				get(JIDX.LEFT_SHOULDER), get(JIDX.RIGHT_SHOULDER));
	}

	public PVector upperUpvec()
	{
		PVector upvec = PVector.sub(get(JIDX.NECK), get(JIDX.TORSO));
		upvec.div(upvec.mag());
		return upvec;
	}

	public PVector lowerUpvec()
	{
		return PVecUtilities.UPVEC;
	}

	@Override
	public Object clone()
	{
		Body cln = new Body();

		cln.setTimestamp(getTimestamp());

		for (JIDX jidx : keySet())
		{
			cln.set(jidx, PVecUtilities.clone(get(jidx)));
		}

		return cln;
	}

	// interpolates a position of the body from the positions and their
	// coefficients given in parameter
	public static Body interpolate(Body body1, Body body2, float coefficient)
	{

		// position of the body to be returned
		Body body = new Body();

		// to store the part of the body which position is being computed
		JIDX iJidx;

		// browses all parts of the body
		for (Map.Entry<JIDX, PVector> joints_iterator : body1.entrySet())
		{

			// part of the body to interpolate
			iJidx = joints_iterator.getKey();

			// interpolates the position of this part of the body and stores it
			body.set(iJidx, PVector.lerp(joints_iterator.getValue(),
					body2.get(iJidx), coefficient));

		}

		// the interpolated position of the body is returned
		return body;

	}
	
	// Calculates the centroid of the body from all bodies in an ArrayList<Body>
	public static Body centroid(ArrayList<Body> bodies)
	{
		// position of the body to be returned
		Body body = (Body)bodies.get(0).clone();

		// to store the part of the body which position is being computed
		JIDX iJidx;

		for(int i = 1; i < bodies.size(); i++)
		{
			Body body1 = bodies.get(i);
			// browses all parts of the body
			for (Map.Entry<JIDX, PVector> joints_iterator : body1.entrySet())
			{
				//Incremental average
				//mt = mt-1 + (at - mt-1) / t
				// part of the body to centroid
				iJidx = joints_iterator.getKey();
				PVector prototypeJoint = body.get(iJidx);
				PVector currentJoint = body1.get(iJidx);
				
				body.set(iJidx, PVector.add(prototypeJoint, PVector.div(PVector.sub(currentJoint, prototypeJoint), i)).get());
			}
		}

		// the centroid position of the body is returned
		return body;

	}

	public boolean isSimilar(Body other, float relativeDeviationTolerance)
	{
		PVector centerA = get(JIDX.TORSO);
		PVector centerB = other.get(JIDX.TORSO);
		for (JIDX jidx : JIDX.ALL_JIDX)
		{
			if (JIDX.TORSO == jidx)
				continue;
			PVector radialA = PVector.sub(get(jidx), centerA);
			PVector radialB = PVector.sub(other.get(jidx), centerB);
			PVector deviation = PVector.sub(radialB, radialA);
			float referenceMag = Math.max(radialA.mag(), radialB.mag());
			float deviationMag = deviation.mag();
			if (deviationMag > referenceMag * relativeDeviationTolerance)
			{
				return false;
			}
		}
		return true;
	}
	
	public float difference(Body other)
	{
		PVector centerA = get(JIDX.TORSO);
		PVector centerB = other.get(JIDX.TORSO);
		float deviationMag = 0f;
		for (JIDX jidx : JIDX.ALL_JIDX)
		{
			if (JIDX.TORSO == jidx)
				continue;
			PVector radialA = PVector.sub(get(jidx), centerA);
			PVector radialB = PVector.sub(other.get(jidx), centerB);
			PVector deviation = PVector.sub(radialB, radialA);
			deviationMag += Math.abs(deviation.mag());
		}
		return deviationMag;
	}

	public float characteristicSize()
	{
		float sumsize = 0.0f;

		for (Pair<JIDX, JIDX> jidxpair : LIDX.LIMB_ENDS().values())
		{
			sumsize += PVector.sub(get(jidxpair.second), get(jidxpair.first)).mag();
		}

		return sumsize / LIDX.LIMB_ENDS().size();
	}

	public Body scale(float factor)
	{
		Body scaled = new Body();

		for (JIDX jidx : JIDX.ALL_JIDX)
		{
			scaled.set(jidx, PVector.mult(get(jidx), factor));
		}

		scaled.setTimestamp(getTimestamp());
		return scaled;
	}
	
	public String toString()
	{
		String text = "[Body: [Timestamp: " + getTimestamp() + ", Joints: [";
		JIDX[] jointIndices = JIDX.ALL_JIDX;
		text += "" + jointIndices[0].toString() + ": " + get(jointIndices[0]).toString();
		for(int index = 1; index < jointIndices.length; index++)
		{
			text += ", " + jointIndices[index].toString() + ": " + get(jointIndices[index]).toString();
		}
		
		text += "]]]";
		return text;
	}
	
	public Body getIdealUserPose()
	{
		Body idealUserPose = new Body();
		//Set Skeleton Coordinates for ideal user Body
		idealUserPose.set(JIDX.HEAD,new PVector(61.86306f, 800.0279f, 1792.6035f));
		idealUserPose.set(JIDX.NECK,new PVector(49.31178f, 482.05545f, 1871.1399f));
		idealUserPose.set(JIDX.LEFT_SHOULDER,new PVector(-117.19889f, 473.39166f, 1895.8171f));
		idealUserPose.set(JIDX.LEFT_ELBOW,new PVector(-234.26807f, 214.76712f, 1940.3701f));
		idealUserPose.set(JIDX.LEFT_HAND,new PVector(-502.1734f, 154.27269f, 1752.4586f));
		idealUserPose.set(JIDX.LEFT_FINGERTIP,new PVector(61.86306f, 800.0279f, 1792.6035f));
		idealUserPose.set(JIDX.RIGHT_SHOULDER,new PVector(215.82245f, 490.7192f, 1846.4626f));
		idealUserPose.set(JIDX.RIGHT_ELBOW,new PVector(380.55743f, 234.44539f, 1845.75f));
		idealUserPose.set(JIDX.RIGHT_HAND,new PVector(641.58655f, 210.31554f, 1670.9052f));
		idealUserPose.set(JIDX.RIGHT_FINGERTIP,new PVector(61.86306f, 800.0279f, 1792.6035f));
		idealUserPose.set(JIDX.TORSO,new PVector(56.47932f, 240.32893f, 1834.6368f));
		idealUserPose.set(JIDX.LEFT_HIP,new PVector(-43.330074f, -6.9635806f, 1813.9879f));
		idealUserPose.set(JIDX.LEFT_KNEE,new PVector(-31.571592f, -493.167f, 1824.4634f));
		idealUserPose.set(JIDX.LEFT_FOOT,new PVector(18.669317f, -967.7855f, 1913.0208f));
		idealUserPose.set(JIDX.RIGHT_HIP,new PVector(170.6238f, 4.1684284f, 1782.2798f));
		idealUserPose.set(JIDX.RIGHT_KNEE,new PVector(193.66336f, -464.59735f, 1739.4097f));
		idealUserPose.set(JIDX.RIGHT_FOOT,new PVector(221.68254f, -936.3788f, 1838.4629f));
		idealUserPose = translated(new PVector(0, 0, idealUserPose.center().z));
		return idealUserPose;
	}
}
