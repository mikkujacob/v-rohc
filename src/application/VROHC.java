package application;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

import com.vrohc.clustering.ClusterMappings;
import com.vrohc.clustering.ClusterResults;
import com.vrohc.executive.ClusteringOptionConstants.DistanceMeasure;
import com.vrohc.executive.ClusteringOptionConstants.ExternalQualityMeasure;
import com.vrohc.executive.ClusteringOptionConstants.InternalQualityMeasure;
import com.vrohc.executive.Executive;
import com.vrohc.executive.ClusteringOptionConstants.ClusteringAlgorithm;
import com.vrohc.executive.ClusteringOptionConstants.EncodingDimensionalityReduction;
import com.vrohc.visualization.Radial;

import input.FileMotionInput;
import input.KinectMotionInput;
import input.MotionInput;
import SimpleOpenNI.SimpleOpenNI;
import processing.core.PApplet;
import processing.core.PVector;
import segmentation.StillnessSegmentation;
import shared.Body;
import shared.Clock;
import shared.JIDX;
import shared.LIDX;
import shared.Pair;
import shared.TempoIndependentMovement;

public class VROHC extends PApplet
{
	/**
	 * Default generated Serial Version UID. Please do not change.
	 */
	private static final long serialVersionUID = 5563536837918268812L;
	
	private static final Boolean isFullScreen = true;
	private static Boolean USE_KINECT = true;
	private static Boolean DRAW_CLUSTERS = false;
	private static Boolean DRAW_USER = true;
	private static Boolean DRAW_GUI = true;
	private static Boolean DEBUG_VISIBLE = true;
	private static Boolean DRAW_GRAPHS = false;
	private static Boolean DRAW_GRAPH_VELOCITY = false;
	private static Boolean DRAW_GRAPH_VELOCITY_SMOOTHED = false;
	private static Boolean IS_RECORDING = false;
	private static Boolean PAUSE_USER_MODE_ON_CLUSTERING = false;
	private static final String CAPTURED_MOVEMENT_DIR = "data" + File.separator + "recorded-movements"
			+ File.separator;
	private static final String CAPTURED_INPUT_SESSION_DIR = "data" + File.separator + "recorded-input-sessions"
			+ File.separator;
	private static String recordedInputSession = "input-session.session";

	private ArrayList<Body> currentMovement;
	private MotionInput motionInput = null;
	private KinectMotionInput kinectInput;
	private FileMotionInput fileInput;
	private float maxZ = 5000;
	private float minZ = 500;
	private Clock clock;
	private StillnessSegmentation segmenter;
	private Executive executive;
	private ArrayList<String> listGUIText;
	private ArrayList<Long> listGUIDuration;
	private ClusterMappings mappings;
	private LinkedBlockingQueue<ClusterResults> resultQueue;
	private ClusterResults result;
	private Radial radialVisualizer;
	private int ClusteringParameterValues;

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		if (isFullScreen)
		{
			PApplet.main(new String[]
			{ "--present", "application.VROHC" });
		}
		else
		{
			PApplet.main(new String[]
			{ "application.VROHC" });
		}
	}

	//COMMENTED AND NOT REMOVED IN CASE PROCESSING 3.0+ IS EVER SUPPORTED BY SIMPLEOPENNI 1.96
//	public void settings()
//	{
//		if(isFullScreen)
//		{
//			setSize(displayWidth, displayHeight);
//		}
//		smooth();
//	}

	public void setup()
	{
		if(isFullScreen)
		{
			size(displayWidth, displayHeight);
		}
		smooth();
		
		clock = new Clock();
		clock.start();
		kinectInput = new KinectMotionInput(new SimpleOpenNI(this), minZ, maxZ);
		fileInput = new FileMotionInput(CAPTURED_INPUT_SESSION_DIR
				+ recordedInputSession);

		if (USE_KINECT)
		{
			if (kinectInput.init())
			{
				motionInput = kinectInput;
			}
		}
		else
		{
			if (fileInput.init())
			{
				motionInput = fileInput;
			}
		}
		
		listGUIText = new ArrayList<String>();
		listGUIDuration = new ArrayList<Long>();
		
		background(0, 0, 0);

		currentMovement = new ArrayList<Body>();
		resultQueue = new LinkedBlockingQueue<ClusterResults>();

		segmenter = new StillnessSegmentation();

		mappings = new ClusterMappings();
		
		radialVisualizer = new Radial(this, mappings, clock);

		ClusteringParameterValues = 0;
		
		executive = new Executive(this, mappings, resultQueue);
		executive.setParameters(ClusteringAlgorithm.BIRCH, EncodingDimensionalityReduction.JOINT_SPACE_POSITIONAL_MOVEMENT_CLIPPED, DistanceMeasure.NONE, ExternalQualityMeasure.NONE, InternalQualityMeasure.NONE);
//		executive.setParameters(ClusteringAlgorithm.BIRCH, EncodingDimensionalityReduction.TEMPORAL_TEMPLATE, DistanceMeasure.NONE, ExternalQualityMeasure.NONE, InternalQualityMeasure.NONE);
//		executive.setParameters(ClusteringAlgorithm.COBWEB, EncodingDimensionalityReduction.JOINT_SPACE_POSITIONAL_MOVEMENT_CLIPPED, DistanceMeasure.NONE, ExternalQualityMeasure.NONE, InternalQualityMeasure.NONE);
//		executive.setParameters(ClusteringAlgorithm.COBWEB, EncodingDimensionalityReduction.TEMPORAL_TEMPLATE, DistanceMeasure.NONE, ExternalQualityMeasure.NONE, InternalQualityMeasure.NONE);
		new Thread(executive).start();
	}

	public void draw()
	{
		if (motionInput == null || !motionInput.isInit())
		{
			return;
		}
		else
		{
			pushMatrix();
			
			clock.check();
			noStroke();
			fill(0, 0, 0, 150);
			rect(0, 0, displayWidth, displayHeight);

			if(DRAW_USER)
			{
				
				Body userPose = motionInput.next(clock);
				
				if(userPose != null)
				{
					if (IS_RECORDING)
					{
						currentMovement.add(userPose);
					}
	
					//Do gesture segmentation
	
					segmenter.update(userPose);
					
					if(DEBUG_VISIBLE)
					{
						addGUIText("VELOCITY: " + segmenter.getLastVelocity(), 100);
						addGUIText("SMOOTHED VELOCITY: " + segmenter.getLastVelocitySmoothed(), 100);
						addGUIText("SEGMENT TIME: " + segmenter.getSegmentTime(), 100);
						addGUIText("PARAMETERS: " + segmenter.getParameters(), 100);
					}
					
					if (segmenter.isSegmentDetected())
					{
						TempoIndependentMovement movement = new TempoIndependentMovement(
								segmenter.consumeSegment());
						TempoIndependentMovement.save(
								CAPTURED_MOVEMENT_DIR + movement.getID() + ".movement",
								movement);
	
						// Send segmented gesture to Clustering module
						executive.setMovement(movement);
					}
					
					// Get segmented gesture clusters from Clustering module
					result = resultQueue.poll();
					
					if(result != null && !IS_RECORDING && PAUSE_USER_MODE_ON_CLUSTERING)
					{
						DRAW_CLUSTERS = true;
						DRAW_USER = false;
						addGUIText("DRAW CLUSTERS: " + DRAW_CLUSTERS, 500);
						addGUIText("DRAW USER: " + DRAW_USER, 500);
					}
	
					if (DRAW_USER)
					{
						drawBody(userPose);
					}
				}
			}
			
			if (DRAW_CLUSTERS)
			{
				// TODO: Visualize segmented gesture clusters
				
				if(result != null)
				{
					radialVisualizer.setClusters(result);
					result = null;
				}
				
				if(radialVisualizer.isInitialized())
				{
					radialVisualizer.draw();
				}
			}
			
			if (DRAW_GUI)
			{
				displayGUI();
			}
			
			if(DRAW_GRAPHS)
			{
				displayGraphs();
			}
			
			popMatrix();
		}
	}

	public void drawBody(Body body)
	{
		pushMatrix();
		translate(width/2f, height/2f);
		int color = g.strokeColor;
		stroke(255);
		strokeWeight(2);

		for (Pair<JIDX, JIDX> limbEnds : LIDX.LIMB_ENDS().values())
		{
			PVector pos1 = getScreenPos(body.get(limbEnds.first));
			PVector pos2 = getScreenPos(body.get(limbEnds.second));
			line(pos1.x, pos1.y, pos2.x, pos2.y);
		}
		stroke(color);
		popMatrix();
	}

	private PVector getScreenPos(PVector pos)
	{
		PVector screenPos = PVector.mult(pos.get(), 0.3f); // new PVector();
		screenPos.x = -screenPos.x;
		screenPos.y = -screenPos.y;
		screenPos.z = (height / 900.0f) * 800.0f / screenPos.z;
		screenPos.x *= screenPos.z;
		screenPos.y *= screenPos.z;
		return screenPos;
	}
	
	@SuppressWarnings("unused")
	private Body getScreenPos(Body pose)
	{
		Body screenPose = (Body) pose.clone();
		for (JIDX jidx : JIDX.ALL_JIDX)
		{
			PVector tempVector = getScreenPos(screenPose.get(jidx).get());
			
			screenPose.set(jidx, tempVector.get());
		}
		
		return screenPose;
	}
	
	public void addGUIText(String message, long duration)
	{
		for (int index = 0; index < listGUIText.size(); index++)
		{
			String testMessage = listGUIText.get(index);
			if (message.split(":")[0].equalsIgnoreCase(testMessage.split(":")[0]))
			{
				listGUIText.remove(index);
				listGUIDuration.remove(index);
				break;
			}
		}

		this.listGUIText.add(message.toUpperCase());
		this.listGUIDuration.add(System.currentTimeMillis() + duration);
	}

	public void displayGUI()
	{
		pushMatrix();
		int oldFill = g.fillColor;
		fill(255,255,255, 200);
		long timeNowMillis = System.currentTimeMillis();
		textSize(32);
		float xPosPercent = 2f / 100f * (float) displayWidth;
		float yPosPercent = 3f / 100f * (float) displayHeight;
		yPosPercent += 5f / 100f * (float) displayHeight;
		for (int index = 0; index < listGUIText.size(); index++)
		{
			String message = listGUIText.get(index);
			long displayTime = listGUIDuration.get(index);
			text(message, xPosPercent, yPosPercent, 75f);
			yPosPercent += 5f / 100f * (float) displayHeight;
			if (displayTime < timeNowMillis)
			{
				listGUIText.remove(index);
				listGUIDuration.remove(index);
			}
		}
		fill(oldFill);
		popMatrix();
	}
	
	public void displayGraphs()
	{
		if (DRAW_GRAPH_VELOCITY && !segmenter.isEmpty())
		{
			displayGraph(segmenter.getCurrentTimestampStream(),
					segmenter.getCurrentVelocityStream(), "Time (s)",
					"Velocity (mm)", segmenter.getFirstTimeStamp(),
					segmenter.getLastTimeStamp(), 0, 200, 0, width, 0, height/2, color(255, 255, 0, 200));
		}

		if (DRAW_GRAPH_VELOCITY_SMOOTHED && !segmenter.isEmpty())
		{
			displayGraph(segmenter.getCurrentTimestampStream(),
					segmenter.getCurrentVelocityStreamSmoothed(), "Time (s)",
					"Velocity Smoothed (mm)", segmenter.getFirstTimeStamp(),
					segmenter.getLastTimeStamp(), 0, 200, 0, width, height/2, height, color(255, 0, 0, 200));
		}
	}

	public void displayGraph(ArrayList<Double> xValues,
			ArrayList<Double> yValues, String xLabel, String yLabel,
			double xMin, double xMax, double yMin, double yMax,
			double screenXMin, double screenxMax, double screenYMin,
			double screenYMax, int color)
	{
		if (yValues.isEmpty())
		{
			return;
		}

		pushMatrix();
		int oldStroke = g.strokeColor;
		stroke(color);
		int oldFill = g.fillColor;
		fill(color);
		
		displayGrid(screenXMin, screenxMax, screenYMin, screenYMax);
		
		double y1 = yMax - yValues.get(0);
		double x1 = xValues.get(0);
		double y2 = yMax - yValues.get(0);
		double x2 = xValues.get(0);

		ellipse((float) (scaleRange(x1, xMin, xMax, screenXMin, screenxMax)),
				(float) (scaleRange(y1, yMin, yMax, screenYMin, screenYMax)),
				10, 10);

		for (int index = 1; index < yValues.size(); index++)
		{
			y1 = yMax - yValues.get(index - 1);
			x1 = xValues.get(index - 1);
			y2 = yMax - yValues.get(index);
			x2 = xValues.get(index);

			line((float) (scaleRange(x1, xMin, xMax, screenXMin, screenxMax)),
					(float) (scaleRange(y1, yMin, yMax, screenYMin, screenYMax)),
					(float) (scaleRange(x2, xMin, xMax, screenXMin, screenxMax)),
					(float) (scaleRange(y2, yMin, yMax, screenYMin, screenYMax)));

			ellipse((float) (scaleRange(x2, xMin, xMax, screenXMin, screenxMax)),
					(float) (scaleRange(y2, yMin, yMax, screenYMin, screenYMax)),
					10, 10);
		}

		fill(oldFill);
		stroke(oldStroke);
		popMatrix();
	}

	public void displayGrid(double xMin, double xMax, double yMin, double yMax)
	{
		double yQuarter = (yMax - yMin) / 4 + yMin;
		double yHalf = (yMax - yMin) / 2 + yMin;
		double y3Quarter = (yMax - yMin) * 3 / 4 + yMin;
		double xQuarter = (xMax - xMin) / 4 + xMin;
		double xHalf = (xMax - xMin) / 2 + xMin;
		double x3Quarter = (xMax - xMin) * 3 / 4 + xMin;
		
		line((float) xMin, (float) yMin, (float) xMax, (float) yMin);
		line((float) xMin, (float) yQuarter, (float) xMax, (float) yQuarter);
		line((float) xMin, (float) yHalf, (float) xMax, (float) yHalf);
		line((float) xMin, (float) y3Quarter, (float) xMax, (float) y3Quarter);
		line((float) xMin, (float) yMax, (float) xMax, (float) yMax);
		
		line((float) xMin, (float) yMin, (float) xMin, (float) yMax);
		line((float) xQuarter, (float) yMin, (float) xQuarter, (float) yMax);
		line((float) xHalf, (float) yMin, (float) xHalf, (float) yMax);
		line((float) x3Quarter, (float) yMin, (float) x3Quarter, (float) yMax);
		line((float) xMax, (float) yMin, (float) xMax, (float) yMax);
	}
	
	public double scaleRange(double oldValue, double oldMin, double oldMax, double newMin, double newMax)
	{
		return (((oldValue - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
	}
	
	public void setResult(ClusterResults result)
	{
		resultQueue.add(result);
	}

	public void onNewUser(SimpleOpenNI context, int userId)
	{
		System.out.println("onNewUser - userId: " + userId);
		segmenter.reset();
		context.startTrackingSkeleton(userId);
		kinectInput.getKinect().startTrackingSkeleton(userId);
	}

	public void onLostUser(SimpleOpenNI context, int userId)
	{
		System.out.println("onLostUser - userId: " + userId);
	}

	public void keyPressed()
	{
		if (key == 'v' || key == 'V')
		{
			DRAW_CLUSTERS = true;
			DRAW_USER = false;
			addGUIText("DRAW CLUSTERS: " + DRAW_CLUSTERS, 500);
			addGUIText("DRAW USER: " + DRAW_USER, 500);
		}

		if (key == 'u' || key == 'U')
		{
			DRAW_USER = true;
			DRAW_CLUSTERS = false;
			addGUIText("DRAW USER: " + DRAW_USER, 500);
			addGUIText("DRAW CLUSTERS: " + DRAW_CLUSTERS, 500);
		}

		if (key == 'r' || key == 'R')
		{
			if (!IS_RECORDING)
			{
				currentMovement.clear();
				IS_RECORDING = true;
			}
			else
			{
				IS_RECORDING = false;
				if (currentMovement != null && !currentMovement.isEmpty())
				{
					TempoIndependentMovement recordedSession = new TempoIndependentMovement(
							currentMovement);
					TempoIndependentMovement.save(CAPTURED_INPUT_SESSION_DIR
							+ UUID.randomUUID() + ".session", recordedSession);
				}
			}
			addGUIText("IS RECORDING: " + IS_RECORDING, 500);
		}
		
		if(key == 'a' || key == 'A')
		{
			if(ClusteringParameterValues == 0)
			{
				executive.reset();
				executive.setParameters(ClusteringAlgorithm.BIRCH, EncodingDimensionalityReduction.TEMPORAL_TEMPLATE, DistanceMeasure.NONE, ExternalQualityMeasure.NONE, InternalQualityMeasure.NONE);
				addGUIText("ALGORITHM: BIRCH", 500);
				addGUIText("ENCODING: TEMPORAL TEMPLATES", 500);
			}
			else if(ClusteringParameterValues == 1)
			{
				executive.reset();
//				executive.setParameters(ClusteringAlgorithm.BIRCH, EncodingDimensionalityReduction.JOINT_SPACE_POSITIONAL_MOVEMENT_CLIPPED, DistanceMeasure.NONE, ExternalQualityMeasure.NONE, InternalQualityMeasure.NONE);
				addGUIText("ALGORITHM: BIRCH", 500);
				addGUIText("ENCODING: JOINT SPACE POSITIONAL CLIPPED", 500);
			}
			else if(ClusteringParameterValues == 2)
			{
				executive.reset();
				executive.setParameters(ClusteringAlgorithm.COBWEB, EncodingDimensionalityReduction.TEMPORAL_TEMPLATE, DistanceMeasure.NONE, ExternalQualityMeasure.NONE, InternalQualityMeasure.NONE);
//				executive.setParameters(ClusteringAlgorithm.BIRCH, EncodingDimensionalityReduction.TEMPORAL_TEMPLATE, DistanceMeasure.NONE, ExternalQualityMeasure.NONE, InternalQualityMeasure.NONE);
				addGUIText("ALGORITHM: COBWEB", 500);
				addGUIText("ENCODING: TEMPORAL TEMPLATES", 500);
			}
			else if(ClusteringParameterValues == 3)
			{
				executive.reset();
				executive.setParameters(ClusteringAlgorithm.COBWEB, EncodingDimensionalityReduction.JOINT_SPACE_POSITIONAL_MOVEMENT_CLIPPED, DistanceMeasure.NONE, ExternalQualityMeasure.NONE, InternalQualityMeasure.NONE);
//				executive.setParameters(ClusteringAlgorithm.BIRCH, EncodingDimensionalityReduction.JOINT_SPACE_POSITIONAL_MOVEMENT_CLIPPED, DistanceMeasure.NONE, ExternalQualityMeasure.NONE, InternalQualityMeasure.NONE);
				addGUIText("ALGORITHM: COBWEB", 500);
				addGUIText("ENCODING: JOINT SPACE POSITIONAL CLIPPED", 500);
			}
			
			ClusteringParameterValues = (ClusteringParameterValues + 1) % 4;
		}

		if (key == 'k' || key == 'K')
		{
			USE_KINECT = !USE_KINECT;

			if (USE_KINECT)
			{
				if (!kinectInput.isInit())
				{
					kinectInput.init();

				}
				motionInput = kinectInput;
			}
			else
			{
				if (!fileInput.isInit())
				{
					fileInput.init();

				}
				motionInput = fileInput;
			}
			addGUIText("KINECT MODE: " + USE_KINECT, 500);
		}
		
		if(key == 'g' || key == 'G')
		{
			DRAW_GUI = !DRAW_GUI;
			addGUIText("DRAW_GUI: " + DRAW_GUI, 500);
		}
		
		if(key == 'd' || key =='D')
		{
			DEBUG_VISIBLE = !DEBUG_VISIBLE;
			addGUIText("DEBUG_VISIBLE: " + DEBUG_VISIBLE, 500);
		}
		
		if(key == '1')
		{
			DRAW_GRAPH_VELOCITY = !DRAW_GRAPH_VELOCITY;
			addGUIText("DRAW GRAPH VELOCITY: " + DRAW_GRAPH_VELOCITY, 500);
		}
		
		if(key == '2')
		{
			DRAW_GRAPH_VELOCITY_SMOOTHED = !DRAW_GRAPH_VELOCITY_SMOOTHED;
			addGUIText("DRAW GRAPH VELOCITY SMOOTHED: " + DRAW_GRAPH_VELOCITY_SMOOTHED, 500);
		}
		
		if(DRAW_GRAPH_VELOCITY || DRAW_GRAPH_VELOCITY_SMOOTHED)
		{
			DRAW_GRAPHS = true;
		}
		else
		{
			DRAW_GRAPHS = false;
		}
	}
}
