package input;

import processing.core.*;
import shared.Body;
import shared.Clock;
import SimpleOpenNI.SimpleOpenNI;

public class KinectMotionInput implements MotionInput
{
	private SimpleOpenNI kinect = null;
	private boolean isInit = false;
	private int userNum = -1;
	private float maxZ, minZ;

	public KinectMotionInput(SimpleOpenNI kinect)
	{
		this(kinect, 300, 5000);
	}

	public KinectMotionInput(SimpleOpenNI kinect, float minZ, float maxZ)
	{
		this.minZ = minZ;
		this.maxZ = maxZ;
		this.kinect = kinect;
	}

	public boolean init()
	{
		if (!kinect.init())
			return false;
		isInit = kinect.enableDepth();
		isInit = isInit && kinect.enableUser();
		return isInit;
	}

	public boolean isInit()
	{
		return isInit;
	}

	public boolean isAnyoneOnScreen(PGraphics pgraphics)
	{
		PImage userimage = kinect.userImage();
		userimage.loadPixels();
		for (int i = 0; i < userimage.pixels.length; i++)
		{
			int color = userimage.pixels[i];
			float r = pgraphics.red(color);
			float g = pgraphics.blue(color);
			float b = pgraphics.green(color);
			if (r != g || g != b)
			{
				return true;
			}
		}
		return false;
	}

	public Body next(Clock clock)
	{
		kinect.update();
		int[] userList = kinect.getUsers();
		int nearestI = -1;
		float nearestZ = Float.MAX_VALUE;
		for (int i = 0; i < userList.length; i++)
		{
			if (kinect.isTrackingSkeleton(userList[i]))
			{
				PVector jointPos = new PVector();
				kinect.getJointPositionSkeleton(userList[i],
						SimpleOpenNI.SKEL_NECK, jointPos);
				if (jointPos.z > minZ && jointPos.z < maxZ
						&& jointPos.z < nearestZ)
				{
					nearestI = i;
					nearestZ = jointPos.z;
				}
			}
		}
		if (nearestI > -1)
		{
			userNum = userList[nearestI];
			Body userPose = new Body();
			userPose.initialize(kinect, userList[nearestI], clock.time());
			return userPose;
		}
		return null;
	}

	public SimpleOpenNI getKinect()
	{
		return kinect;
	}

	public int getUser()
	{
		return userNum;
	}
}
