package input;

import shared.Body;
import shared.Clock;
import shared.TempoIndependentMovement;

public class FileMotionInput implements MotionInput
{
	private String filename = null;
	private boolean isInit = false;
	private TempoIndependentMovement motionRecord = null;

	public FileMotionInput(String filename)
	{
		this.filename = filename;
	}

	public boolean init()
	{
		motionRecord = TempoIndependentMovement.load(filename);
		if (motionRecord != null)
		{
			isInit = true;
		}
		return isInit;
	}

	public boolean isMovementFile()
	{
		if (filename.substring(filename.length() - 3).equals("movement"))
		{
			return true;
		}
		return false;
	}

	public boolean isInit()
	{
		return isInit;
	}

	public void setReplaySpeed(float f)
	{
		motionRecord.setReplaySpeed(f);
	}

	public Body next(Clock clock)
	{
		float deltatime = clock.deltatime();
		return motionRecord.replayFrame(deltatime);
	}

	public Body next(float f)
	{
		return motionRecord.at(f);
	}

	public void repeat(boolean repeat)
	{
		motionRecord.setIsRepetitionOn(repeat);
	}

	public float getTime()
	{
		return motionRecord.getTimePlayed();
	}

	public float getDuration()
	{
		return motionRecord.getBasicDuration();
	}
}
