package input;

import shared.Body;
import shared.Clock;

public interface MotionInput
{
	public boolean init();

	public boolean isInit();

	public Body next(Clock clock);
}
