package com.vrohc.similarity;

public class EuclideanDistance
{
	public static double getEuclideanSimilarity(Double[] X, Double[] Y)
	{
		if(X.length != Y.length)
		{
			return -1;
		}
		
		return 1 / (getEuclideanDistance(X, Y) + 1);
	}
	
	public static double getEuclideanDistance(Double[] X, Double[] Y)
	{
		if(X.length != Y.length)
		{
			return -1;
		}
		
		return Math.sqrt(SquaredEuclideanDistance.getSquaredEuclideanDistance(X, Y));
	}
}
