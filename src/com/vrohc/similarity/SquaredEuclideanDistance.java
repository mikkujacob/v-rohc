/**
 * 
 */
package com.vrohc.similarity;

/**
 * @author mikhailjacob
 *
 */
public class SquaredEuclideanDistance
{
	public static double getSquaredEuclideanSimilarity(Double[] X, Double[] Y)
	{
		if(X.length != Y.length)
		{
			return -1;
		}
		
		return 1 / (getSquaredEuclideanDistance(X, Y) + 1);
	}
	
	public static double getSquaredEuclideanDistance(Double[] X, Double[] Y)
	{
		if(X.length != Y.length)
		{
			return -1;
		}
		
		double sum = 0.0;
		
		for(int i = 0; i < X.length; i++)
		{
			sum += (X[i] - Y[i]) * (X[i] - Y[i]);
		}
		
		return sum;
	}
}
