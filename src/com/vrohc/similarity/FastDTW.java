/**
 * 
 */
package com.vrohc.similarity;

import com.dtw.TimeWarpInfo;
import com.dtw.WarpPath;
import com.dtw.timeseries.TimeSeries;
import com.dtw.util.DistanceFunction;
import com.dtw.util.DistanceFunctionFactory;

/**
 * @author mikhailjacob
 *
 */
public class FastDTW
{
	private TimeSeries ts1;
	private TimeSeries ts2;
	private TimeWarpInfo warpInfo;
	private WarpPath warpPath;
	
	public enum SupportedDistanceMetrics
	{
		EuclideanDistance,
		ManhattanDistance,
		BinaryDistance
	};
	
	public double getDistance(double[] t1, double[] t2, int dimension, SupportedDistanceMetrics distanceMetric, int searchRadius)
	{
		ts1 = new TimeSeries(t1, dimension);
		ts2 = new TimeSeries(t2, dimension);
		
		DistanceFunction distFn = DistanceFunctionFactory.getDistFnByName(distanceMetric.toString());
		
		warpInfo = com.dtw.FastDTW.getWarpInfoBetween(ts1, ts2, searchRadius, distFn);
		warpPath = warpInfo.getPath();
		
		return warpInfo.getDistance();
	}
	
	public double getDistance()
	{
		if(warpInfo != null)
		{
			return warpInfo.getDistance();
		}
		
		return -1;
	}
	
	public WarpPath getPath()
	{
		if(warpPath != null)
		{
			return warpPath;
		}
		
		return null;
	}
}
