package com.vrohc.similarity;

public class JaccardIndex
{
	public static double getJaccardDistance(Double[] X, Double[] Y)
	{
		if(X.length != Y.length)
		{
			return -1;
		}
		
		return 1 - getJaccardSimilarity(X, Y);
	}
	
	public static double getJaccardSimilarity(Double[] X, Double[] Y)
	{
		if(X.length != Y.length)
		{
			return -1;
		}
		
		double maxSum = 0.0;
		double minSum = 0.0;
		
		for(int i = 0; i < X.length; i++)
		{
			minSum += Math.min(X[i], Y[i]);
			maxSum += Math.max(X[i], Y[i]);
		}
		
		return (minSum / maxSum);
	}
}
