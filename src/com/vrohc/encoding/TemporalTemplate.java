package com.vrohc.encoding;

import java.util.Arrays;
import java.util.UUID;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PVector;
import shared.Body;
import shared.DataTypeConversions;
import shared.JIDX;
import shared.LIDX;
import shared.Pair;
import shared.TempoIndependentMovement;

/**
 * @author mikhailjacob
 *
 */
public class TemporalTemplate extends SerializableEncoding
{
	/**
	 * Generated Serial Version UID.
	 */
	private static final long serialVersionUID = 1796478066613352446L;
	PApplet parent;
	PGraphics motionEnergyImage;
	PGraphics motionHistoryImage;
	int width;
	int height;

	public TemporalTemplate(TempoIndependentMovement movement, int width,
			int height, PApplet parent)
	{
		super(movement.getID());
		this.parent = parent;
		this.width = width;
		this.height = height;
		
		initializeMotionEnergyImage(movement);
		initializeMotionHistoryImage(movement);
	}
	
	public void initializeMotionEnergyImage(TempoIndependentMovement movement)
	{
		motionEnergyImage = parent.createGraphics(width, height);
		motionEnergyImage.beginDraw();
		motionEnergyImage.loadPixels();
		motionEnergyImage.background(0,0,0);
		for(int index = 0; index <= 100; index++)
		{
			Body pose = movement.at(index * movement.getBasicDuration() / 100);
			motionEnergyImage = drawBody(pose, motionEnergyImage, parent.color(255, 255, 255));
		}
		motionEnergyImage.filter(PApplet.DILATE);
		motionEnergyImage.filter(PApplet.DILATE);
		motionEnergyImage.filter(PApplet.DILATE);
		motionEnergyImage.filter(PApplet.BLUR, 3);
		motionEnergyImage.updatePixels();
		motionEnergyImage.endDraw();
		
		motionEnergyImage.save("data/images/energy/" + UUID.randomUUID() + ".png");
	}
	
	public void initializeMotionHistoryImage(TempoIndependentMovement movement)
	{
		motionHistoryImage = parent.createGraphics(width, height);
		motionHistoryImage.beginDraw();
		motionHistoryImage.loadPixels();
		motionHistoryImage.background(0,0,0);
		
		int oldFill = motionHistoryImage.fillColor;
		motionHistoryImage.fill(parent.color(0,0,0,2));
		for(int index = 0; index <= 100; index++)
		{
			motionHistoryImage.rect(0, 0, width, height);
			Body pose = movement.at(index * movement.getBasicDuration() / 100);
			motionHistoryImage = drawBody(pose, motionHistoryImage, parent.color(255, 255, 255));
		}
		motionHistoryImage.fill(oldFill);
		
		motionHistoryImage.filter(PApplet.DILATE);
		motionHistoryImage.filter(PApplet.DILATE);
		motionHistoryImage.filter(PApplet.DILATE);
		motionHistoryImage.filter(PApplet.BLUR, 3);
		
		motionHistoryImage.updatePixels();
		motionHistoryImage.endDraw();
		
		motionHistoryImage.save("data/images/history/" + UUID.randomUUID() + ".png");
	}

	public PGraphics drawBody(Body body, PGraphics image, int color)
	{
		image.stroke(color);
		image.strokeWeight(10);

		for (Pair<JIDX, JIDX> limbEnds : LIDX.LIMB_ENDS().values())
		{
			PVector pos1 = getScreenPos(body.get(limbEnds.first));
			pos1.x += width / 2;
			pos1.y += height / 2;
			PVector pos2 = getScreenPos(body.get(limbEnds.second));
			pos2.x += width / 2;
			pos2.y += height / 2;
			image.line(pos1.x+5, pos1.y, pos2.x+5, pos2.y);
			image.line(pos1.x, pos1.y, pos2.x, pos2.y);
			image.line(pos1.x-5, pos1.y, pos2.x-5, pos2.y);
		}
		
		return image;
	}

	private PVector getScreenPos(PVector pos)
	{
		PVector screenPos = PVector.mult(pos.get(), 0.3f); // new PVector();
		screenPos.x = -screenPos.x;
		screenPos.y = -screenPos.y;
		float z = (height / 900.0f) * 800.0f / screenPos.z;
		screenPos.x *= z;
		screenPos.y *= z;
		return screenPos;
	}

	private PVector getWorldPos(PVector pos)
	{
		PVector worldPos = new PVector(pos.x, pos.y, pos.z);
		worldPos.x = -worldPos.x;
		worldPos.y = -worldPos.y;
		float z = (height / 900.0f) * 800.0f / worldPos.z;
		worldPos.x /= z;
		worldPos.y /= z;
		worldPos = PVector.div(worldPos, 0.3f);
		return worldPos;
	}
	

	public void test(TempoIndependentMovement movement)
	{
		for (float i = 0f; i < movement.getBasicDuration(); i++)
		{
			Body pose = movement.at(i);
			System.out.println("Pose: " + pose.toString());
			for (PVector joint : pose.values())
			{
				System.out.println("Original: " + joint.toString());
				PVector screenJoint = getScreenPos(joint);
				System.out.println("Screen: " + screenJoint.toString());
				PVector realJoint = getWorldPos(screenJoint);
				System.out.println("Real: " + realJoint.toString());
				System.out.println();
			}
		}
	}

	public void finalize()
	{
		// TODO Auto-generated method stub

	}

	public void initialize(TempoIndependentMovement movement)
	{
		// TODO Auto-generated method stub

	}
	
	public PGraphics getMotionEnergyImage()
	{
		return motionEnergyImage;
	}
	
	public PGraphics getMotionHistoryImage()
	{
		return motionHistoryImage;
	}
	
	public PGraphics getMotionEnergyImageScaled(float scale)
	{
		PGraphics result = parent.createGraphics(width, height);
		result.loadPixels();
		result.pixels = Arrays.copyOf(motionEnergyImage.pixels, motionEnergyImage.pixels.length);
		result.updatePixels();
		result.resize(Math.round(width * scale), Math.round(height * scale));
		return result;
	}
	
	public PGraphics getMotionHistoryImageScaled(float scale)
	{
		PGraphics result = parent.createGraphics(width, height);
		result.loadPixels();
		result.pixels = Arrays.copyOf(motionHistoryImage.pixels, motionHistoryImage.pixels.length);
		result.updatePixels();
		result.resize(Math.round(width * scale), Math.round(height * scale));
		return result;
	}
	
	public PGraphics getTemporalTemplate()
	{
		return getTemporalTemplate(motionEnergyImage, motionHistoryImage);
	}
	
	public double[] getTemporalTemplateData()
	{
		return DataTypeConversions.intArraytoDoubleArray(getTemporalTemplate(motionEnergyImage, motionHistoryImage).pixels);
	}
	
	private PGraphics getTemporalTemplate(PGraphics energyImage, PGraphics historyImage)
	{
		PGraphics result = parent.createGraphics(energyImage.width, energyImage.height);
		result.beginDraw();
		result.loadPixels();
		int[] first, second;
		
		first = Arrays.copyOf(energyImage.pixels, energyImage.pixels.length);
		second = Arrays.copyOf(historyImage.pixels, historyImage.pixels.length);
		
		for(int i = 0; i < result.pixels.length; i++)
		{
			result.pixels[i] = result.lerpColor(first[i], second[i], 0.5f);
		}
		
		result.updatePixels();
		result.endDraw();
		return result;
	}
	
	public PGraphics getTemporalTemplateScaled(float scale)
	{
		PGraphics result = parent.createGraphics(width, height);
		result.loadPixels();
		result.pixels = Arrays.copyOf(motionHistoryImage.pixels, motionHistoryImage.pixels.length);
		result.updatePixels();
		result.resize(Math.round(width * scale), Math.round(height * scale));
		PGraphics result2 = parent.createGraphics(width, height);
		result2.loadPixels();
		result2.pixels = Arrays.copyOf(motionHistoryImage.pixels, motionHistoryImage.pixels.length);
		result2.updatePixels();
		result2.resize(Math.round(width * scale), Math.round(height * scale));
		return getTemporalTemplate(result, result2);
	}
	
	public double[] getTemporalTemplateScaledData(float scale)
	{
		PGraphics temporalTemplateImage =  getTemporalTemplateScaled(0.1f);
		temporalTemplateImage.save("data/images/temporaltemplates/" + UUID.randomUUID() + ".png");
		
		return DataTypeConversions.intArraytoDoubleArray(temporalTemplateImage.pixels);
	}
}
