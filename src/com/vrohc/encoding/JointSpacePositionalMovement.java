/**
 * 
 */
package com.vrohc.encoding;

import java.util.ArrayList;

import shared.Body;
import shared.TempoIndependentMovement;

/**
 * @author mikhailjacob
 *
 */
public class JointSpacePositionalMovement extends SerializableEncoding
{
	/**
	 * Generated Serial Version UID.
	 */
	private static final long serialVersionUID = 4745707001588411695L;
	ArrayList<Body> poses;
	public static final int DATA_LENGTH = 101;
	
	public JointSpacePositionalMovement(TempoIndependentMovement movement)
	{
		super(movement.getID());
		poses = new ArrayList<Body>();
		for(int index = 0; index < DATA_LENGTH; index++)
		{
			poses.add(movement.at(index * movement.getBasicDuration() / (DATA_LENGTH - 1)));
		}
	}

	public ArrayList<Body> getPoses()
	{
		return poses;
	}

	public void setPoses(ArrayList<Body> poses)
	{
		this.poses = poses;
	}
}
