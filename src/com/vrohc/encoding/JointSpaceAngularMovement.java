/**
 * 
 */
package com.vrohc.encoding;

import shared.TempoIndependentMovement;

/**
 * @author mikhailjacob
 *
 */
public class JointSpaceAngularMovement extends SerializableEncoding
{

	/**
	 * General Serial Version UID.
	 */
	private static final long serialVersionUID = -1440550138024033795L;

	public JointSpaceAngularMovement(TempoIndependentMovement movement)
	{
		// TODO Auto-generated constructor stub
		super(movement.getID());
	}

}
