/**
 * 
 */
package com.vrohc.encoding;

import shared.TempoIndependentMovement;

/**
 * @author mikhailjacob
 *
 */
public class JointSpaceAngularMovementClipped extends SerializableEncoding
{

	/**
	 * Generated Serial Version UID.
	 */
	private static final long serialVersionUID = -5042707390508902398L;

	public JointSpaceAngularMovementClipped(TempoIndependentMovement movement)
	{
		// TODO Auto-generated constructor stub
		super(movement.getID());
	}

}
