/**
 * 
 */
package com.vrohc.encoding;

import java.util.Arrays;

import processing.core.PVector;
import shared.Body;
import shared.JIDX;
import shared.TempoIndependentMovement;

/**
 * @author mikhailjacob
 *
 */
public class JointSpacePositionalMovementClipped extends SerializableEncoding
{
	/**
	 * Generated Serial Version UID.
	 */
	private static final long serialVersionUID = -1854579061504870279L;
	double[] data;
	public static final int DATA_LENGTH = 11;
	public static final int DIMENSIONS = (new PVector()).array().length;
	
	public JointSpacePositionalMovementClipped(TempoIndependentMovement movement)
	{
		super(movement.getID());
		data = new double[DIMENSIONS * DATA_LENGTH];
		for(int index = 0; index < DATA_LENGTH; index++)
		{
			Body pose = movement.at(index * movement.getBasicDuration() / (DATA_LENGTH - 1));
			float[] position = clipBody(pose);
			for(int i = 0; i < DIMENSIONS; i++)
			{
				data[DIMENSIONS * index + i] = position[i];
			}
		}
		System.out.println("Clipped Movement: " + Arrays.toString(data));
	}
	
	private float[] clipBody(Body pose)
	{
		PVector jointCentroid = new PVector();
		
//		System.out.println("Body: " + pose.toString());
		
		for(JIDX jidx : JIDX.ALL_JIDX)
		{
			jointCentroid.add(pose.get(jidx));
		}
		
		jointCentroid.div(JIDX.ALL_JIDX.length);
		
//		System.out.println("Clipped Body: " + jointCentroid.toString());
		
		return jointCentroid.array();
	}

	public double[] getData()
	{
		return data;
	}

	public void setData(double[] data)
	{
		this.data = data;
	}
}
