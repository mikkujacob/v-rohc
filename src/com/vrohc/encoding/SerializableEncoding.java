package com.vrohc.encoding;

import java.io.Serializable;
import java.util.UUID;

public class SerializableEncoding implements Serializable
{
	/**
	 * Default Serial Version UID. Subclasses must generate their own.
	 */
	private static final long serialVersionUID = 1L;
	private UUID ID;
	
	public SerializableEncoding()
	{
		this.ID = UUID.randomUUID();
	}
	
	public SerializableEncoding(UUID ID)
	{
		this.ID = ID;
	}
	
	public UUID getID()
	{
		return ID;
	}
	
	public void setID(UUID ID)
	{
		this.ID = ID;
	}
}
