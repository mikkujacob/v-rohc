package com.vrohc.prototype;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import shared.Body;
import shared.TempoIndependentMovement;

import com.vrohc.clustering.ClusterMappings;

public class ClusterPrototype
{
	ClusterMappings mappings;
	private static final int TOTAL_BODY_COUNT = 101;
	
	public ClusterPrototype(ClusterMappings mappings)
	{
		this.mappings = mappings;
	}
	
	public TempoIndependentMovement getPrototype(List<Integer> clusteredObjectIndices)
	{
		ArrayList<TempoIndependentMovement> movements = new ArrayList<TempoIndependentMovement>();
		mappings.printAll();
		for(int index = 0; index < clusteredObjectIndices.size(); index++)
		{
			String name = mappings.getobjectUUID(clusteredObjectIndices.get(index)).toString();
			//Load the movement
			System.out.println("Loading: " + ClusterMappings.CAPTURED_MOVEMENT_DIR + name + ".movement");
			TempoIndependentMovement movement = TempoIndependentMovement.load(ClusterMappings.CAPTURED_MOVEMENT_DIR + name + ".movement");
			
			//Add it to the movement list
			movements.add(movement);
		}
		
		return getPrototype(movements);
	}
	
	public TempoIndependentMovement getPrototype(ArrayList<TempoIndependentMovement> movements)
	{
		ArrayList<Body> movementPrototype = new ArrayList<Body>();
		//For each body across each cluster
		for(int bodyIndex = 0; bodyIndex < TOTAL_BODY_COUNT; bodyIndex++)
		{
			ArrayList<Body> prototypeBodyList = new ArrayList<Body>();
			//For each cluster index
			for(int index = 0; index < movements.size(); index++)
			{
				TempoIndependentMovement movement = movements.get(index);
				
				//Add it to the body list
				prototypeBodyList.add(movement.at((float)(bodyIndex * movement.getBasicDuration())));
				System.out.println("Interpolation Coefficient: " + (float)(bodyIndex * movement.getBasicDuration()));
//				System.out.println("Duration: " + movement.getBasicDuration());
			}
			
			movementPrototype.add((Body)Body.centroid(prototypeBodyList).clone());
		}
		
		//Garbage collection
		System.gc();
		
		//Return the prototype
		System.out.println("Prototype Body List: " + movementPrototype.toString());
		return new TempoIndependentMovement(movementPrototype);
	}
}
