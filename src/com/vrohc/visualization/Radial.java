package com.vrohc.visualization;

import java.util.ArrayList;

import com.vrohc.clustering.ClusterMappings;
import com.vrohc.clustering.ClusterResults;
import com.vrohc.prototype.ClusterPrototype;

import processing.core.PApplet;
import processing.core.PVector;
import shared.Body;
import shared.Clock;
import shared.JIDX;
import shared.LIDX;
import shared.Pair;
import shared.TempoIndependentMovement;

public class Radial
{
	private PApplet parent;
	private ArrayList<TempoIndependentMovement> prototypeMovements;
	private TempoIndependentMovement rootMovementPrototype;
	private ClusterResults clusters;
	private ClusterMappings mappings;
	private ClusterPrototype prototyper;
	private Clock clock;
	private boolean isInitialized;
	
	public Radial(PApplet parent, ClusterMappings mappings, Clock clock)
	{
		this.parent = parent;
		this.mappings = mappings;
		this.clock = clock;
		prototyper = new ClusterPrototype(mappings);
		prototypeMovements = new ArrayList<TempoIndependentMovement>();
	}
	
	public void draw()
	{
		parent.pushMatrix();
		int oldFill = parent.g.fillColor;
		int oldStroke = parent.g.strokeColor;
		float strokeWeight = parent.g.strokeWeight;
		
		float baseRadius = parent.height / 4f;
		float radius = baseRadius + 0.1f * parent.height;
		float angle = 0;
		//Calculate positioning of each movement display
		for(int index = 0; index < prototypeMovements.size(); index++)
		{
			Body body = nextBody(prototypeMovements.get(index));
			angle = (float)(-1 * Math.PI / 2) + index * (float)(2 * Math.PI / prototypeMovements.size());
			
			float centerX = (float)(radius * Math.cos(angle));
			float centerY = (float)(radius * Math.sin(angle));
			
			System.out.println("Radius: " + radius + ", Angle: " + angle);
			System.out.println("CenterX: " + centerX + ", CenterY: " + centerY);
			
			drawBody(body, centerX, centerY, 0.25f, 0f, centerX, centerY);
		}
		
		//Draw root prototype
		Body body = nextBody(rootMovementPrototype);
		drawBody(body, 0, 0, 0.5f, 0f, parent.width / 2f, parent.height / 2f);
		
		parent.fill(oldFill);
		parent.stroke(oldStroke);
		parent.strokeWeight(strokeWeight);
		parent.popMatrix();
	}
	
	public Body nextBody(TempoIndependentMovement movement)
	{
		System.out.println("Clock Init Time: " + clock.iniTime());
		System.out.println("Time In Current Cycle: " + movement.getTimeInCurrentCycle());
		return movement.replayFrame(clock.deltatime());
	}
	
	public void drawBody(Body body, float translateX, float translateY, float scaleFactor, float rotateAngle, float rotateCenterX, float rotateCenterY)
	{
		parent.pushMatrix();
		
		parent.translate(parent.width/2f, parent.height/2f);
		
		parent.translate(translateX, translateY);
		
		parent.translate(rotateCenterX, rotateCenterY);
		parent.rotate(rotateAngle);
		parent.translate(-1 * rotateCenterX,-1 * rotateCenterY);
		
		Body scaledBody = (Body)body.clone();
		scaledBody = scaledBody.scale(scaleFactor);
		
		int color = parent.g.strokeColor;
		parent.stroke(255);
		parent.strokeWeight(10);

		for (Pair<JIDX, JIDX> limbEnds : LIDX.LIMB_ENDS().values())
		{
			PVector pos1 = getScreenPos(body.get(limbEnds.first));
			pos1.mult(scaleFactor);
			PVector pos2 = getScreenPos(body.get(limbEnds.second));
			pos2.mult(scaleFactor);
			parent.line(pos1.x, pos1.y, pos2.x, pos2.y);
		}
		parent.stroke(color);
		parent.popMatrix();
	}

	private PVector getScreenPos(PVector pos)
	{
		PVector screenPos = PVector.mult(pos.get(), 0.3f); // new PVector();
		screenPos.x = -screenPos.x;
		screenPos.y = -screenPos.y;
		screenPos.z = (parent.height / 900.0f) * 800.0f / screenPos.z;
		screenPos.x *= screenPos.z;
		screenPos.y *= screenPos.z;
		return screenPos;
	}
	
	@SuppressWarnings("unused")
	private Body getScreenPos(Body pose)
	{
		Body screenPose = (Body) pose.clone();
		for (JIDX jidx : JIDX.ALL_JIDX)
		{
			PVector tempVector = getScreenPos(screenPose.get(jidx).get());
			
			screenPose.set(jidx, tempVector.get());
		}
		
		return screenPose;
	}
	
	public void setClusters(ClusterResults newClusters)
	{
		clusters = newClusters;
		prototypeMovements.clear();
		for(int index = 0; index < clusters.getClusterIDList().size(); index++)
		{
			prototypeMovements.add(prototyper.getPrototype(clusters.getClusterIDList().get(index)));
		}
		rootMovementPrototype = prototyper.getPrototype(prototypeMovements);
		isInitialized = true;
	}

	public boolean isInitialized()
	{
		return isInitialized;
	}

	public void setInitialized(boolean isInitialized)
	{
		this.isInitialized = isInitialized;
	}
}
