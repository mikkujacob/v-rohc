package com.vrohc.clustering;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.UUID;

public class ClusterMappings
{
	public static final String CAPTURED_MOVEMENT_DIR = "data" + File.separator + "recorded-movements"
			+ File.separator;
	
	private LinkedHashMap<Integer, UUID> clusterIntegerIDtoUUID;
	private LinkedHashMap<UUID, Integer> clusterUUIDtointegerID;
	private LinkedHashMap<Integer, UUID> objectIntegerIDtoUUID;
	private LinkedHashMap<UUID, Integer> objectUUIDtointegerID;
	
	public ClusterMappings()
	{
		clusterIntegerIDtoUUID = new LinkedHashMap<Integer, UUID>();
		clusterUUIDtointegerID = new LinkedHashMap<UUID, Integer>();
		objectIntegerIDtoUUID = new LinkedHashMap<Integer, UUID>();
		objectUUIDtointegerID = new LinkedHashMap<UUID, Integer>();
	}
	
	public UUID getClusterUUID(Integer intID)
	{
		return clusterIntegerIDtoUUID.get(intID);
	}
	
	public Integer getClusterIntegerID(UUID ID)
	{
		return clusterUUIDtointegerID.get(ID);
	}
	
	public UUID getobjectUUID(Integer intID)
	{
		return objectIntegerIDtoUUID.get(intID);
	}
	
	public Integer getObjectIntegerID(UUID ID)
	{
		return objectUUIDtointegerID.get(ID);
	}
	
	public void setClusterIDPair(UUID ID, Integer intID)
	{
		clusterUUIDtointegerID.put(ID, intID);
		clusterIntegerIDtoUUID.put(intID, ID);
	}
	
	public void setClusterIDPair(Integer intID)
	{
		setClusterIDPair(UUID.randomUUID(), intID);
	}
	
	public void setClusterIDPair(UUID ID)
	{
		setClusterIDPair(ID, clusterUUIDtointegerID.size());
	}
	
	public void setObjectIDPair(UUID ID, Integer intID)
	{
		objectUUIDtointegerID.put(ID, intID);
		objectIntegerIDtoUUID.put(intID, ID);
	}
	
	public void setObjectIDPair(Integer intID)
	{
		setObjectIDPair(UUID.randomUUID(), intID);
	}
	
	public void setObjectIDPair(UUID ID)
	{
		setObjectIDPair(ID, objectUUIDtointegerID.size() + 1);
	}
	
	public void printAll()
	{
		System.out.println("Cluster Integer ID to UUID:");
		Iterator<Entry<Integer, UUID>> IntegertoIDIterator = clusterIntegerIDtoUUID.entrySet().iterator();
		while(IntegertoIDIterator.hasNext())
		{
			Entry<Integer, UUID> entry = IntegertoIDIterator.next();
			System.out.println(entry.getKey() + ":" + entry.getValue());
		}
		
		System.out.println("\nCluster UUID to Integer:");
		Iterator<Entry<UUID, Integer>> IDtoIntegerIterator = clusterUUIDtointegerID.entrySet().iterator();
		while(IDtoIntegerIterator.hasNext())
		{
			Entry<UUID, Integer> entry = IDtoIntegerIterator.next();
			System.out.println(entry.getKey() + ":" + entry.getValue());
		}
		
		System.out.println("\nObject Integer ID to UUID:");
		IntegertoIDIterator = objectIntegerIDtoUUID.entrySet().iterator();
		while(IntegertoIDIterator.hasNext())
		{
			Entry<Integer, UUID> entry = IntegertoIDIterator.next();
			System.out.println(entry.getKey() + ":" + entry.getValue());
		}
		
		System.out.println("\nObject UUID to Integer:");
		IDtoIntegerIterator = objectUUIDtointegerID.entrySet().iterator();
		while(IDtoIntegerIterator.hasNext())
		{
			Entry<UUID, Integer> entry = IDtoIntegerIterator.next();
			System.out.println(entry.getKey() + ":" + entry.getValue());
		}
		System.out.println();
	}
	
	public void clearAll()
	{
		clusterIntegerIDtoUUID.clear();
		clusterUUIDtointegerID.clear();
		objectIntegerIDtoUUID.clear();
		objectUUIDtointegerID.clear();
	}
}
