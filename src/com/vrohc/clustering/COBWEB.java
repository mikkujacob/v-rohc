/**
 * 
 */
package com.vrohc.clustering;

import weka.clusterers.Cobweb;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

/**
 * @author mikhailjacob
 *
 */
public class COBWEB
{
	private Cobweb clusterer;
	private boolean isInitialized;
	
	public COBWEB()
	{
		clusterer = new Cobweb();
		isInitialized = false;
	}
	
	public void buildModel(double[] attributes)
	{
		
		FastVector attributevector = new FastVector(attributes.length);
		
		for(int i = 0; i < attributes.length; i++)
		{
			attributevector.addElement(new Attribute("attributeNumeric" + i));
		}
		Instances instances = new Instances("Movement", attributevector, 1);
		Instance instance = new Instance(1.0, attributes);
		
		instances.add(instance);
		try
		{
			clusterer.buildClusterer(instances);
			isInitialized = true;
			System.gc();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.gc();
		}
	}
	
	public void updateModel(double[] attributes)
	{
		if(!isInitialized)
		{
			return;
		}
		
		FastVector attributevector = new FastVector(attributes.length);
		
		for(int i = 0; i < attributes.length; i++)
		{
			attributevector.addElement(new Attribute("attributeNumeric" + i));
		}
		Instances instances = new Instances("Movement", attributevector, 1);
		Instance instance = new Instance(1.0, attributes);
		
		instances.add(instance);
		
//		Instance newInstance = new Instance(1.0, attributes);
		try
		{
//			clusterer.updateClusterer(newInstance);
			clusterer.updateClusterer(instances.firstInstance());
			System.gc();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.gc();
		}
	}
	
	public int testModel(double[] attributes)
	{
		if(!isInitialized)
		{
			return -1;
		}
		
		FastVector attributevector = new FastVector(attributes.length);
		
		for(int i = 0; i < attributes.length; i++)
		{
			attributevector.addElement(new Attribute("attributeNumeric" + i));
		}
		Instances instances = new Instances("Movement", attributevector, 1);
		Instance instance = new Instance(1.0, attributes);
		
		instances.add(instance);
		
		try
		{
//			return clusterer.clusterInstance(new Instance(1.0, attributes));
			return clusterer.clusterInstance(instances.firstInstance());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.gc();
			return -1;
		}
	}
	
	public boolean isInitialized()
	{
		return isInitialized;
	}
	
	public String clustererToString()
	{
		return clusterer.toString();
	}
	
	public Cobweb getCobweb()
	{
		return clusterer;
	}
}
