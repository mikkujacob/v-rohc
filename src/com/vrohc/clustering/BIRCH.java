/**
 * 
 */
package com.vrohc.clustering;

import com.jbirch.cftree.CFTree;

/**
 * @author mikhailjacob
 *
 */
public class BIRCH
{
	private CFTree birchTree;
	
	private int maxNodeEntries;
	private double distThreshold;
	private int distFunction = CFTree.D0_DIST;
	private boolean applyMergingRefinement = true;
	private boolean automaticRebuild = true;
	private int memoryLimit; // in MB
	private int memoryLimitPeriodicCheck; // verify memory usage after every 10000 inserted instances 
	
	public BIRCH()
	{
		this(10, 300.0, CFTree.D4_DIST, true, true, 1, 10);
	}
	
	public BIRCH(Integer maxNodeEntries, Double distThreshold, Integer distFunction, Boolean applyMergingRefinement, Boolean automaticRebuild, Integer memoryLimit, Integer memoryLimitPeriodicCheck)
	{
		if(maxNodeEntries != null)
			this.maxNodeEntries = maxNodeEntries;
		if(distThreshold != null)
			this.distThreshold = distThreshold;
		if(distFunction != null)
			this.distFunction = distFunction;
		if(applyMergingRefinement != null)
			this.applyMergingRefinement = applyMergingRefinement;
		if(automaticRebuild != null)
			this.automaticRebuild = automaticRebuild;
		if(memoryLimit != null)
			this.memoryLimit = memoryLimit;
		if(memoryLimitPeriodicCheck != null)
			this. memoryLimitPeriodicCheck = memoryLimitPeriodicCheck;
		
		birchTree = createNewCFTree();
		birchTree.setAutomaticRebuild(isAutomaticRebuild());
		birchTree.setMemoryLimitMB(getMemoryLimit());
		birchTree.setPeriodicMemLimitCheck(getMemoryLimitPeriodicCheck());
	}
	
	private CFTree createNewCFTree()
	{
		return new CFTree(maxNodeEntries, distThreshold, distFunction, applyMergingRefinement);
	}
	
	public boolean insertEntry(double[] x)
	{
		boolean inserted = birchTree.insertEntry(x);
		birchTree.finishedInsertingData();
		return inserted;
	}
	
	public boolean insertEntry(double[][] xArray)
	{
		boolean inserted = false;
		for(double[] x : xArray)
		{
			inserted = inserted && birchTree.insertEntry(x);
		}
		birchTree.finishedInsertingData();
		return inserted;
	}

	public CFTree getBirchTree()
	{
		return birchTree;
	}

	public void setBirchTree(CFTree birchTree)
	{
		this.birchTree = birchTree;
	}

	public int getMaxNodeEntries()
	{
		return maxNodeEntries;
	}

	public void setMaxNodeEntries(int maxNodeEntries)
	{
		this.maxNodeEntries = maxNodeEntries;
	}

	public double getDistThreshold()
	{
		return distThreshold;
	}

	public void setDistThreshold(double distThreshold)
	{
		this.distThreshold = distThreshold;
	}

	public int getDistFunction()
	{
		return distFunction;
	}

	public void setDistFunction(int distFunction)
	{
		this.distFunction = distFunction;
	}

	public boolean isApplyMergingRefinement()
	{
		return applyMergingRefinement;
	}

	public void setApplyMergingRefinement(boolean applyMergingRefinement)
	{
		this.applyMergingRefinement = applyMergingRefinement;
	}

	public int getMemoryLimit()
	{
		return memoryLimit;
	}

	public void setMemoryLimit(int memoryLimit)
	{
		this.memoryLimit = memoryLimit;
	}

	public int getMemoryLimitPeriodicCheck()
	{
		return memoryLimitPeriodicCheck;
	}

	public void setMemoryLimitPeriodicCheck(int memoryLimitPeriodicCheck)
	{
		this.memoryLimitPeriodicCheck = memoryLimitPeriodicCheck;
	}

	public boolean isAutomaticRebuild()
	{
		return automaticRebuild;
	}

	public void setAutomaticRebuild(boolean automaticRebuild)
	{
		this.automaticRebuild = automaticRebuild;
	}
}
