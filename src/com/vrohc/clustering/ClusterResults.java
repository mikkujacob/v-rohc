package com.vrohc.clustering;

import java.util.ArrayList;

import com.vrohc.executive.ClusteringOptionConstants.ClusteringAlgorithm;

public class ClusterResults
{
	ArrayList<ArrayList<Integer>> clusterIDList;
	Object resultObject;
	ClusteringAlgorithm algorithm;
	
	public ArrayList<ArrayList<Integer>> getClusterIDList()
	{
		return clusterIDList;
	}
	
	public void setClusterIDList(ArrayList<ArrayList<Integer>> clusterIDList)
	{
		this.clusterIDList = clusterIDList;
	}
	
	public Object getResultObject()
	{
		return resultObject;
	}
	
	public void setResultObject(Object resultObject)
	{
		this.resultObject = resultObject;
	}
	
	public ClusteringAlgorithm getAlgorithm()
	{
		return algorithm;
	}
	
	public void setAlgorithm(ClusteringAlgorithm algorithm)
	{
		this.algorithm = algorithm;
	}
}
