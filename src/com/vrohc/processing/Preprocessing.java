/**
 * 
 */
package com.vrohc.processing;

import java.util.ArrayList;

import processing.core.PVector;
import shared.Body;
import shared.PVecUtilities;
import shared.TempoIndependentMovement;

/**
 * @author mikhailjacob
 *
 */
public class Preprocessing
{
	public static TempoIndependentMovement preprocessMotion(
			TempoIndependentMovement movement)
	{
		ArrayList<Body> poses = new ArrayList<Body>();
		for (int i = 0; i <= 100; i++)
		{
			Body pose = movement.at(i * movement.getBasicDuration() / 100f);
			PVector standardCenter = new PVector(0, 0, pose.center().z);
			// 1. Translate all pose centers to center of frame
			// 2. Scale all poses to uniform size

			float characteristicSize = pose.characteristicSize();
			PVector[] targetBasis = new PVector[3];
			for (int j = 0; j < 3; j++)
			{
				targetBasis[j] = PVector.mult(PVecUtilities.STANDARD_BASIS[j],
						pose.getIdealUserPose().characteristicSize() / characteristicSize);
			}
			
			pose.setTimestamp(i * movement.getBasicDuration() / 100f);

			poses.add(pose.transformed(pose.localBasis(), targetBasis, standardCenter));
		}

		return new TempoIndependentMovement(poses);
	}
}
