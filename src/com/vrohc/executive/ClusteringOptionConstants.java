package com.vrohc.executive;

public class ClusteringOptionConstants
{
	public enum ClusteringAlgorithm
	{
		COBWEB,
		BIRCH,
		CUSTOM,
		NONE
	};
	
	public enum EncodingDimensionalityReduction
	{
		TEMPORAL_TEMPLATE,
		JOINT_SPACE_POSITIONAL_MOVEMENT,
		JOINT_SPACE_POSITIONAL_MOVEMENT_CLIPPED,
		JOINT_SPACE_ANGULAR_MOVEMENT, //TODO
		JOINT_SPACE_ANGULAR_MOVEMENT_CLIPPED, //TODO
		NONE
	};
	
	public enum DistanceMeasure
	{
		EUCLIDEAN,
		EUCLIDEAN_SQUARED,
		JACCARD,
		FAST_DTW,
		DTW,
		LCSS, //TODO
		NONE
	};
	
	public enum ExternalQualityMeasure
	{
		CLUSTER_SIMILARITY, //TODO
		F1, //TODO
		NORMALIZED_MUTUAL_INFORMATION, //TODO
		ADJUSTED_RAND_INDEX, //TODO
		NONE
	};
	
	public enum InternalQualityMeasure
	{
		SUM_OF_SQUARED_ERROR, //TODO
		CLUSTER_DISTANCE, //TODO
		ROOT_MEAN_SQUARED_STANDARD_DEVIATION, //TODO
		NONE
	};
}
