package com.vrohc.executive;

import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

import com.vrohc.clustering.BIRCH;
import com.vrohc.clustering.COBWEB;
import com.vrohc.clustering.ClusterMappings;
import com.vrohc.clustering.ClusterResults;
import com.vrohc.encoding.JointSpacePositionalMovementClipped;
import com.vrohc.encoding.TemporalTemplate;
import com.vrohc.executive.ClusteringOptionConstants.ClusteringAlgorithm;
import com.vrohc.executive.ClusteringOptionConstants.DistanceMeasure;
import com.vrohc.executive.ClusteringOptionConstants.EncodingDimensionalityReduction;
import com.vrohc.executive.ClusteringOptionConstants.ExternalQualityMeasure;
import com.vrohc.executive.ClusteringOptionConstants.InternalQualityMeasure;
import com.vrohc.processing.Preprocessing;

import processing.core.PApplet;
import shared.TempoIndependentMovement;

public class Executive implements Runnable
{
	private PApplet parent;
	private ClusterMappings clusteringIDMappings;
	private TempoIndependentMovement movement;
	private double[] movementData;
	private LinkedBlockingQueue<TempoIndependentMovement> inputQueue;
	private LinkedBlockingQueue<ClusterResults> outputQueue;
	private COBWEB cobwebClusterer;
	private BIRCH birchClusterer;
	
	private ClusteringAlgorithm algorithm;
	private EncodingDimensionalityReduction encoding;
	private DistanceMeasure distance;
	private ExternalQualityMeasure externalQuality;
	private InternalQualityMeasure internalQuality;

	public Executive(PApplet parent, ClusterMappings clusterMappings, LinkedBlockingQueue<ClusterResults> outputQueue)
	{
		this.parent = parent;
		clusteringIDMappings = clusterMappings;
		inputQueue = new LinkedBlockingQueue<TempoIndependentMovement>();
		this.outputQueue = outputQueue;
		cobwebClusterer = new COBWEB();
		birchClusterer = new BIRCH();
	}

	public void processData()
	{
		TempoIndependentMovement movement = null;
		try
		{
			movement = inputQueue.take();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
			return;
		}
		
		// Pre-process gesture
		this.movement = Preprocessing.preprocessMotion(movement);
		
		//Add mapping between object UUID and object integer ID pair
		clusteringIDMappings.setObjectIDPair(movement.getID());
		
		// Encode gesture in knowledge representation
		// Reduce dimensionality
		if(encoding == EncodingDimensionalityReduction.TEMPORAL_TEMPLATE)
		{
			doTemporalTemplateEncoding();
		}
		else if(encoding == EncodingDimensionalityReduction.JOINT_SPACE_POSITIONAL_MOVEMENT)
		{
			doJointSpacePositionalMovementEncoding();
		}
		else if(encoding == EncodingDimensionalityReduction.JOINT_SPACE_POSITIONAL_MOVEMENT_CLIPPED)
		{
			doJointSpacePositionalMovementClippedEncoding();
		}
		
		// Attempt clustering
		if(algorithm == ClusteringAlgorithm.COBWEB)
		{
			try
			{
				doCOBWEB();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else if(algorithm == ClusteringAlgorithm.BIRCH)
		{
			try
			{
				doBIRCH();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		// Compare similarity with other members of each cluster
		
		
		// Return gesture clusters
		if(algorithm == ClusteringAlgorithm.COBWEB)
		{
			outputQueue.add(getCOBWEBResults());
		}
		else if(algorithm == ClusteringAlgorithm.BIRCH)
		{
			outputQueue.add(getBIRCHResults());
		}
	}
	
	public void doTemporalTemplateEncoding()
	{
		movementData = (new TemporalTemplate(movement, parent.width,
				parent.height, parent)).getTemporalTemplateScaledData(0.1f);
	}
	
	public void doJointSpacePositionalMovementEncoding()
	{
		
	}
	
	public void doJointSpacePositionalMovementClippedEncoding()
	{
		movementData = (new JointSpacePositionalMovementClipped(movement)).getData();
	}
	
	public void doCOBWEB() throws Exception
	{
		if(movementData == null)
		{
			System.out.println("ERROR! DOUBLE[] DATA IS NULL!");
			throw new Exception("Data is null and empty.");
		}
		
		if(cobwebClusterer.isInitialized())
		{
			System.out.println("Subcluster index predicted: " + cobwebClusterer.testModel(movementData));
			cobwebClusterer.updateModel(movementData);
		}
		else
		{
			cobwebClusterer.buildModel(movementData);
		}
		
		System.out.println("Subcluster index actual: " + cobwebClusterer.testModel(movementData));
		System.out.println("Cobweb Clusterer: " + cobwebClusterer.clustererToString());
//		System.out.println("Graph: " + cobwebClusterer.getCobweb().graph());
	}
	
	public void doBIRCH() throws Exception
	{
		if(this.movementData == null)
		{
			System.out.println("ERROR! DOUBLE[] DATA IS NULL!");
			throw new Exception("Data is null and empty.");
		}
		
		if(birchClusterer.getBirchTree().countEntries() > 0)
		{
			System.out.println("Subcluster index predicted: " + birchClusterer.getBirchTree().mapToClosestSubcluster(movementData));
		}
		
		birchClusterer.insertEntry(movementData);
		
		int clusterIntegerID = birchClusterer.getBirchTree().mapToClosestSubcluster(movementData);
		
		System.out.println("BIRCH Clusterer: ");
		birchClusterer.getBirchTree().printCFTree();
		System.out.println("Subcluster index actual: " + clusterIntegerID);
		System.out.println("Number of Leaf Nodes: " + birchClusterer.getBirchTree().countLeafEntries());
		birchClusterer.getBirchTree().printLeafEntries();
		System.out.println("Total CF-Nodes = " + birchClusterer.getBirchTree().countNodes());
		System.out.println("Total CF-Entries = " + birchClusterer.getBirchTree().countEntries());
		System.out.println("Total CF-Leaf_Entries = " + birchClusterer.getBirchTree().countLeafEntries());
		
		if(clusteringIDMappings.getClusterUUID(clusterIntegerID) == null)
		{
			clusteringIDMappings.setClusterIDPair(UUID.randomUUID(), clusterIntegerID);
		}
		
//		CFTree oldTree = birchClusterer.getBirchTree();
//		CFTree newTree = null;
//		double newThreshold = birchClusterer.getDistThreshold();
//		newThreshold = oldTree.computeNewThreshold(oldTree.getLeafListStart(), birchClusterer.getDistFunction(), newThreshold);
//		System.out.println("new Threshold: " + newThreshold);
		
//		newTree = oldTree.rebuildTree(birchClusterer.getMaxNodeEntries(), newThreshold, birchClusterer.getDistFunction(), true, false);
//		System.out.println("Total CF-Nodes in new Tree: " + newTree.countNodes());
//		System.out.println("Total CF-Entries in new Tree: " + newTree.countEntries());
//		System.out.println("Total CF-Leaf_Entries in new Tree: " + newTree.countLeafEntries());	
//		System.out.println("Total CF-Leaf_Entries lambdaSS in new Tree: " + newTree.computeSumLambdaSquared());
		
//		birchClusterer.setBirchTree(newTree);
//		birchClusterer.setDistThreshold(newThreshold);
	}
	
	public ClusterResults getCOBWEBResults()
	{
		return parseCOBWEBString();
	}
	
	public ClusterResults parseCOBWEBString()
	{
		/*
		 * Example graph:
		 * 
		 * digraph CobwebTree {
		 * N0 [label="node 0  (21)" ]
		 * N0->N1
		 * N0->N2
		 * N0->N3
		 * N0->N4
		 * N0->N5
		 * N0->N6
		 * N0->N7
		 * N0->N8
		 * N0->N9
		 * N0->N10
		 * N0->N11
		 * N0->N12
		 * N0->N13
		 * N0->N14
		 * N0->N15
		 * N0->N16
		 * N0->N17
		 * N0->N18
		 * N0->N19
		 * N0->N20
		 * N0->N21
		 * N1 [label="leaf 1  (1)" shape=box style=filled ]
		 * N2 [label="leaf 2  (1)" shape=box style=filled ]
		 * N3 [label="leaf 3  (1)" shape=box style=filled ]
		 * N4 [label="leaf 4  (1)" shape=box style=filled ]
		 * N5 [label="leaf 5  (1)" shape=box style=filled ]
		 * N6 [label="leaf 6  (1)" shape=box style=filled ]
		 * N7 [label="leaf 7  (1)" shape=box style=filled ]
		 * N8 [label="leaf 8  (1)" shape=box style=filled ]
		 * N9 [label="leaf 9  (1)" shape=box style=filled ]
		 * N10 [label="leaf 10  (1)" shape=box style=filled ]
		 * N11 [label="leaf 11  (1)" shape=box style=filled ]
		 * N12 [label="leaf 12  (1)" shape=box style=filled ]
		 * N13 [label="leaf 13  (1)" shape=box style=filled ]
		 * N14 [label="leaf 14  (1)" shape=box style=filled ]
		 * N15 [label="leaf 15  (1)" shape=box style=filled ]
		 * N16 [label="leaf 16  (1)" shape=box style=filled ]
		 * N17 [label="leaf 17  (1)" shape=box style=filled ]
		 * N18 [label="leaf 18  (1)" shape=box style=filled ]
		 * N19 [label="leaf 19  (1)" shape=box style=filled ]
		 * N20 [label="leaf 20  (1)" shape=box style=filled ]
		 * N21 [label="leaf 21  (1)" shape=box style=filled ]
		 * }
		 * */
		
		ClusterResults results = new ClusterResults();
		try
		{
			String resultString = cobwebClusterer.getCobweb().graph();
			System.out.println("\n" + resultString + "\n");
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}
	
	public ClusterResults getBIRCHResults()
	{
		ClusterResults results = new ClusterResults();
		
		results.setClusterIDList(birchClusterer.getBirchTree().getSubclusterMembers());
		results.setAlgorithm(algorithm);
		
		return results;
	}
	
	public void setMovement(TempoIndependentMovement movement)
	{
		inputQueue.add(movement);
	}
	
	public void setParameters(ClusteringAlgorithm algorithm,
			EncodingDimensionalityReduction encoding,
			DistanceMeasure distance,
			ExternalQualityMeasure externalQuality,
			InternalQualityMeasure internalQuality)
	{
		this.algorithm = algorithm;
		this.encoding = encoding;
		this.distance = distance;
		this.externalQuality = externalQuality;
		this.internalQuality = internalQuality;
	}
	
	public void reset()
	{
		clusteringIDMappings.clearAll();
		movement = null;
		movementData = null;
		inputQueue.clear();
		outputQueue.clear();
		cobwebClusterer = new COBWEB();
		birchClusterer = new BIRCH();
		
		System.gc();
	}

	@Override
	public void run()
	{
		while(true)
		{
			if(algorithm == null ||
				encoding == null || 
				distance == null ||
				externalQuality == null ||
				internalQuality == null)
			{
				continue;
			}
			
			processData();
		}
		
	}
}