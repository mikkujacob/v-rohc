RUNNING THE CODE:

Java binary class file (VROHC.class) is available to run in bin/application/

The library SimpleOpenNI can be downloaded from https://code.google.com/p/simple-openni/downloads/detail?name=SimpleOpenNI-1.96.zip and should be added to the class path

VIEWING AND RUNNING THE CODE FROM ECLIPSE:

1. Download Eclipse from https://eclipse.org/downloads/ .
2. Unzip codebase.
3. Import project using File->Import->General->Existing Projects Into Workspace, then click Next.
4. Select codebase root directory and click Finish.
5. Source is inside src/, libraries are inside lib/, documentation is inside docs/
6. Download the library SimpleOpenNI, unzip it, and add it to the "lib" directory within a folder named SimpleOpenNI-1.96 or somewhere in the Eclipse classpath.
7. Open application.VROHC.java
8. Click Run As Application.